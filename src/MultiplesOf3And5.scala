object MultiplesOf3And5 extends App {

  def times(n: Int, limit: Int) = Math.floor((limit-1)/n).toInt

  def linearSum(n: Int, limit: Int) = {
    val tms = times(n, limit)
    (tms+1) * n * tms/2
  }

  def multiple(limit: Int)(n1: Int, n2: Int) = linearSum(n1, limit) + linearSum(n2, limit) - linearSum(n1*n2, limit)

  //println(linearSum(3, 10)) // 3+6+9 = 18
  //println(linearSum(3, 15)) // 3+6+9+12 = 30
  //println(linearSum(3, 20)) // 3+6+9+12+15+18 = 63

  println(multiple(20)(3, 5)) // 78
  println(multiple(1000)(3, 5)) // 233168


}
